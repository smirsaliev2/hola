import { headerLinksData, articles, navBarLinks, navBarFooterLinks } from "./data.js";

let isMobilePopped = false

document.querySelector('.nav-menu-close-btn').addEventListener('click', toggleNavMenu)

document.querySelector('.burger-btn').addEventListener('click', toggleNavMenu)

jQuery(document).on('scroll', myScrollSpeedFunction );

document.querySelector('.exit-intent-popup').addEventListener('click', exit);

setTimeout(() => {
    if(!jQuery('body').hasClass('on-mobile-device')) {
        document.addEventListener('mouseout', mouseOut);
        document.addEventListener('keydown', exit);
    }
}, 10_000);

// mobile close intent popup
jQuery(document).on('touchstart', function(){
    $('body').addClass('on-mobile-device');
});

function myScrollSpeedFunction(){
    if( jQuery('body').hasClass('on-mobile-device') && !isMobilePopped){ 
        if(my_scroll() < -75){
            //Your code here to display Exit Intent popup
            scrollingUp();
            isMobilePopped = true;
        }
    }
}

var my_scroll = (function(){ //Function that checks the speed of scrolling
    var last_position, new_position, timer, delta, delay = 50; 
    function clear() {
        last_position = null;
        delta = 0;
    }

    clear();
    return function(){
        new_position = window.scrollY;
        if ( last_position != null ){
            delta = new_position -  last_position;
        }
        last_position = new_position;
        clearTimeout(timer);
        timer = setTimeout(clear, delay);
        return delta;
};
})();

function scrollingUp() {
    document.querySelector('.exit-intent-popup').classList.add('visible');
}

// desctop close intent popout 
const mouseOut = e => {
    const shouldShowExitIntent = 
        // for desctop
        !e.toElement && 
        !e.relatedTarget &&
        e.clientY < 10

    if (shouldShowExitIntent) {
        document.removeEventListener('mouseout', mouseOut);
        document.querySelector('.exit-intent-popup').classList.add('visible');
    }   
   
};

// close button exits popout
function exit(e) {
    const shouldExit =
        [...e.target.classList].includes('exit-intent-popup') || // user clicks on mask
        e.target.className === 'close' || // user clicks on the close icon
        e.keyCode === 27; // user hits escape

    if (shouldExit) {
        document.querySelector('.exit-intent-popup').classList.remove('visible');
        document.removeEventListener('mouseout', mouseOut);
    }
};

function toggleNavMenu() {
    document.querySelector('.nav-menu').classList.toggle('hidden')
}

function render() {
    // rendering header navbar links
    let headerNavUlHtml = ''
    for (let link of headerLinksData) {
        headerNavUlHtml += `<li><a href=${link.link}>${link.name}</a></li>`
    }
    document.querySelector('.header-nav ul').innerHTML = headerNavUlHtml

    // rendering articles
    let mainHtml = ''
    for (let article of articles) {
        let articleHtml = `
        <section class="article">
            <div class="section-header">
                <div class="section-title">
                    <h1>${article.h1}</h1>
                    <h2>${article.h2}</h2>
                </div>
                <img class="section-main-img" src="images/article${article.id}/foto1.jpg" alt="2 photos of Adamari Lopez before fit and fit">
            </div>
            <div class="section-content">
                ${article.content.join('\n')}
            </div>
            <div class="comments-container">
                <h2>Comments</h2>
                <div class="comment">
                    <div class="avatar-div">
                        <img src="${article.comments.avatar}" class="avatar">
                    </div>
                    <div class="comment-content">
                        <p class="comment-date">${article.comments.date}</p>
                        <h4 class="comment-name">${article.comments.username}</h4>
                        <p>${article.comments.text}</p>
                        <img src="${article.comments.image}" class='comment-img'>
                    </div>
                </div>
            </div>
        </section>`
        mainHtml += articleHtml
    }
    document.querySelector('main').innerHTML = mainHtml;

    // render nav menu
    let navMenuHtml = ''
    for (let link of navBarLinks) {
        navMenuHtml += `<li><a href="${link.link}">${link.name}</a></li>`
    }
    document.querySelector('.nav-menu-links ul').innerHTML = navMenuHtml

    let navMenuFooterHtml = ''
    for (let link of navBarFooterLinks) {
        navMenuFooterHtml += `<li><a href="${link.link}">${link.name}</a></li>`
    }
    document.querySelector('.nav-menu-footer ul').innerHTML = navMenuFooterHtml

}
           
render()