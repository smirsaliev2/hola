const headerLinksData = [
    {
        name: 'EN INGLES',
        link: '#'
    },
    {
        name: 'CELEBRIDADES',
        link: '#'
    },
    {   
        name: 'REALEZA',
        link: '#'
    },
    {
        name: 'BELLEZA',
        link: '#'
    },
    {
        name: 'MODA',
        link: '#'
    },
    {
        name: 'LIFESTYLE',
        link: '#'
    },
    {
        name: 'LATINA POWERHOUSE',
        link: '#'
    },
    {
        name: 'FOTINA',
        link: ''
    }
]

const navBarLinks = [
    {
        name: 'HOME',
        link: '#'
    },
    {
        name: 'CELEBRITIES',
        link: '#'
    },
    {
        name: 'ENTERTAINMENT',
        link: '#'
    },
    {
        name: 'ROYALS',
        link: '#'
    },
    {
        name: 'HEALTH AND BEAUTY',
        link: '#'
    },
    {
        name: 'FASHION',
        link: '#'
    },
    {
        name: 'LIFESTYLE',
        link: '#'
    }
]

const navBarFooterLinks = [
    {
        name: 'Sitios HOLA!',
        link: '#'
    },
    {
        name: 'Sitios internacionales',
        link: '#'
    },
    {
        name: 'Politica de privacidad',
        link: '#'
    },
    {
        name: 'Configuracion',
        link: '#'
    },
    {
        name: 'Avico legal',
        link: '#'
    },
    {
        name: 'Cookies',
        link: '#'
    },
    {
        name: 'Contacto',
        link: '#'
    }
]

const articles = [
    {
        id: 1,
        h1: '¿QUIERES PERDER 26 KG? ¡ES MUY FÁCIL!',
        h2: 'Los secretos para perder peso de Adamari López',
        content: [
            "<h3>HE PERDIDO PESO YO MISMA Y TE AYUDARÉ A PERDER PESO</h3>",
            '<p>¡Hola a todos!Lo siento, pero simplemente no tuve tiempo de responder todas vuestras preguntas.Si nos ponemos a pensar, es el mismo tema: cómo perder peso de una vez y para siempre. ¡Y por supuesto sin dañar la salud!Así que decidí escribir sobre todo en este post y contar cómo perdí 26 kilos.</p>',
            '<p>Nunca he sido una mujer débil, pero mi cuerpo se volvió loco y tenía gorditos en todas partes: tenía un bocio que colgaba del pecho como un pavo, tenía grasa y celulitis en todas partes, en el estómago, en las piernas e incluso en los brazos. Después de darme cuenta de eso boté todas las camisetas sin mangas.</p>', 
            '<p>Estaba cansada de llorar frente al espejo hasta que pude ponerme en pie de nuevo. ¡Más precisamente para recuperar mi cuerpo gordo! Probé de todo y al mismo tiempo, probablemente como todas las otras chicas ingenuas como yo. Me puse a dieta y comencé a hacer ejercicio. Me despertaba al amanecer, comía un huevo crudo (¡nunca lo hagáis!) E iba a rastras a la calle, apenas movía los pies. Dejé los alimentos fritos, me inscribí en el gimnasio, compré té para bajar de peso y llené el refrigerador con yogur. Tenía todo el paquete, por así decirlo. ¿Creéis que logré algo?</p>',
            "<p>¡Al contrario! Aumenté aún más de peso, de repente subí otros 5 kg en estas semanas que practiqué deporte intensamente. ¡En total llegué a 89 kg! ¡Con un poco más de 1,60 cm de estatura!Más tarde, los médicos me explicaron que el cuerpo estaba bajo estrés, por lo que comenzó a acumular intensamente 'suministros' Mi organismo creía que quería destruirlo. Olvidé el sabor del pan, olvidé qué es la carne, olvidé cómo se derrite el helado en mi boca. Comía solo manzanas y bebía solo agua, pero en todo vano.</p>", 
            '<p>En resumen, durante meses probé diferentes métodos para perder peso hasta que un día me volví loca.</p>',
            "<p>Me avergüenza hablar de esto, pero un día decidí que estaba destinada a morir como una anciana gorda rodeada de 7 gatos. Fui directamente a McDonald's, compré la mitad del menú y me detuve media hora después. Después lloré durante horas, sentí que no podía ser peor.</p>",
            '<p>El paquete llegó rápidamente, en menos de una semana. Durante este tiempo, leí muchas reseñas e información sobre Biolica y me convencí de que era la mejor opción. Seguí las instrucciones, bebí el Biolica dos veces al día, por la mañana y por la noche.</p>',
            "<p>¡En 2 semanas perdí 13 kg! Eso significa que estaba perdiendo alrededor de una libra por día, ¿puedes creerlo?!</p>",
            "<img src='images/article1/foto3.jpg' alt='2 Photos of Adamari Lopes'>",
            "<p>Mis nalgas se han vuelto más delgadas por casi 10 cm, el estómagosimplemente desapareció - no lo creía. Mi piel, cabello: todo se volvió brillante y saludable, las espinillas en la espalda y el pecho desaparecieron desde el principio. Comencé a creer que realmente podía pasar de ser una mujer gorda a una princesa real: ¡la balanza y el espejo no mienten! Y créanme, no seguía ninguna dieta, el éxito de mi pérdida de peso lo logré únicamente con la ayuda de Biolica .</p>",
            "<p>Y la tercera semana fue un éxito: perdí 6 kg de peso. Y lo más sorprendente fue que no tenía que hacer nada, solo bebía Biolica todo según las instrucciones y trataba de recordar tomar la dosis recomendada. Incluso la persona más perezosa puede hacerlo.</p>",
            "<p>En 3 semanas perdí 10 kg. Sin esfuerzo, sin restricciones dietéticas o ejercicio extenuante. Mi panza gorda, mis brazos llenos de celulitis y mi bocio se fueron para siempre. Casi lloro de felicidad mientras escribo esto :)</p>",
            "<p>Después de una dieta completa con Biolica, es decir, después de 2 meses, perdió 26 kg. No sabemos cuántos milagros puede lograr el cuerpo humano si se le ayuda.Ya no tengo 20 años, una edad en la que los kilos se acumulan y se pueden perder rápidamente.</p>",
            "<img src='images/article1/foto5.jpg'>"
        ],
        comments: {
                username: 'Carolina Martín',
                avatar: 'images/comments1/ava1.jpg',
                date: '26.11.2021',
                text: "Desde peque soñaba con un cuerpo delgado. Me enteré sobre Biolica hace tres meses. Dudaba en pedirlo, pero mi madre me convenció. ¡No es realmente costoso hacer realidad tu sueño! Ahora ambas estamos perdiendo peso. Ella también pudo perder peso más que yo, pero los resultados siguen siendo increíbles.",
                image: 'images/comments1/img1.jpg'
        }
    }
]


export { headerLinksData, articles, navBarLinks, navBarFooterLinks }